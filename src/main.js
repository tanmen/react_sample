require('./action/step1');
require('./action/step2');
require('./action/step3');

require('./reducer/step1');
require('./reducer/step2');
require('./reducer/step3');
require('./reducer/step4');

//
//
// console.log('----- NORMAL2 ----');
// const { tanTanmen } = createActions(
//   'TAN_TANMEN'
// );
//
// console.log(tanTanmen());
// console.log(tanTanmen('A'));
// console.log(tanTanmen(1));
// console.log(tanTanmen({ data: 'tanmen' }));
//
//
// console.log('----- PROMISE ----');
// var data = {};
// // console.log(data);
//
// Promise.resolve()
//   .then(() => console.log(data))
//   .delay(5000)
//   .then(() => (data = tanmen('data')))
//   .then(() => console.log(data));