const logger = require('../util/logger');
const { createActions } = require('redux-actions');
const { createStore } = require('redux');

logger('Reducer STEP3');

const Action = {
  REQUEST: 'REQUEST',
  FINISH: 'FINISH',
};

const { request, finish } = createActions(
  Action.REQUEST,
  Action.FINISH);

function reducer(state = {}, action) {
  switch (action.type) {
    case Action.REQUEST: {
      const { payload } = action;
      return Object.assign({}, state, { params: payload });
    }
    case Action.FINISH:
      const { data, page } = action.payload;
      return Object.assign({}, state, { data, page });
    default:
      return state
  }
}

const store = createStore(reducer);

store.subscribe(() =>
  console.log(store.getState())
);

const requestAction = request({ params: { k: 'たんたんめん' } });
console.log(requestAction);
// Action =>
// { type: 'REQUEST', payload: { params: { k: 'たんたんめん' } } }
store.dispatch(requestAction);
// Reducer =>
//  Prev: {}
//  Next: { params: { params: { k: 'たんたんめん' } } }

const finishAction = finish({ data: [{ jobId: 0 }], page: { current: 0, size: 1 } });
console.log(finishAction);
// Action =>
// { type: 'FINISH',
//   payload: { data: [ { jobId: 0 } ], page: { current: 0, size: 1 } } }

store.dispatch(finishAction);
// Reducer =>
//  Prev: { params: { params: { k: 'たんたんめん' } } }
//  Next: { params: { params: { k: 'たんたんめん' } },
//          data: [ { jobId: 0 } ],
//          page: { current: 0, size: 1 } }

