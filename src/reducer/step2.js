const logger = require('../util/logger');
const { createActions } = require('redux-actions');
const { createStore } = require('redux');

logger('Reducer STEP2', 'payload以下のmapping処理つき');

const Action = {
  SET_DATA: 'SET_DATA',
  REMOVE_DATA: 'REMOVE_DATA',
};

const pageMapping = (data, page) => ({ data, page });

const { setData, removeData } = createActions({
  [Action.SET_DATA]: pageMapping,
},
  Action.REMOVE_DATA
);

function reducer(state = {}, action) {
  switch (action.type) {
    case Action.SET_DATA:
      return Object.assign({}, state, action.payload);
    case Action.REMOVE_DATA:
      return {};
    default:
      return state
  }
}

const store = createStore(reducer);

store.subscribe(() =>
  console.log(store.getState())
);

const setDataAction = setData({ jobId: 0 }, { current: 0, size: 1 });
console.log(setDataAction);
// Action =>
// { type: 'SET_DATA',
//   payload: { data: { jobId: 0 }, page: { current: 0, size: 1 } } }

store.dispatch(setDataAction);
// Reducer =>
//  Prev: {}
//  Next: { data: { jobId: 0 }, page: { current: 0, size: 1 } }

store.dispatch(removeData());
// Reducer =>
//  Prev: { data: { jobId: 0 }, page: { current: 0, size: 1 } }
//  Next: {}

