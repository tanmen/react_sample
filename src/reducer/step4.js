const logger = require('../util/logger');
const _ = require('lodash');
const Promise = require("bluebird");
const { createActions } = require('redux-actions');
const { createStore } = require('redux');

logger('Reducer STEP4', '非同期処理サンプル');

const Action = {
  REQUEST: 'REQUEST',
  FINISH: 'FINISH',
};

const { request, finish } = createActions(
  Action.REQUEST,
  Action.FINISH);

function reducer(state = {}, action) {
  switch (action.type) {
    case Action.REQUEST: {
      const { payload } = action;
      return Object.assign({}, state, { params: payload });
    }
    case Action.FINISH:
      const { data } = action.payload;
      return Object.assign({}, state, { data });
    default:
      return state
  }
}

const store = createStore(reducer);

store.subscribe(() =>
  console.log(store.getState())
);


function validateDispatch(params, data) {
  if (_.isEqual(store.getState().params, params)) {
    console.log('\nResult:')
    store.dispatch(finish(data))
  }
}

const ubuntu = () => {
  const params = { k: 'うぶんつ' };
  return Promise.resolve()
    .then(() => store.dispatch(request(params)))
    .delay(100)
    .then(() => {
      validateDispatch(params, { data: 'うぶんつ' });
    })
};

const tanmen =  () => {
  const params = { k: 'たんめん' };
  return Promise.resolve()
    .delay(200)
    .then(() => store.dispatch(request(params)))
    .delay(200)
    .then(() => {
      validateDispatch(params, { data: 'たんめん' });
    })
};

const gebara = () => {
  const params = { k: 'げばら' };
  return Promise.resolve()
    .delay(50)
    .then(() => store.dispatch(request(params)))
    .delay(500)
    .then(() => {
      validateDispatch(params, { data: 'うぶんつ' });
    })
};

Promise
  .all([
    ubuntu(),
    tanmen(),
    gebara(),
  ]);
// =>
//  LastState:
//    { params: { k: 'たんめん' }, data: 'たんめん' }