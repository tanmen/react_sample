const logger = require('../util/logger');
const { createActions } = require('redux-actions');
const { createStore } = require('redux');

logger('Reducer STEP1', 'state書き換えパターン');

const Action = {
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
};

const { increment, decrement } = createActions(
  Action.INCREMENT,
  Action.DECREMENT);

function reducer(state = 0, action) {
  switch (action.type) {
    case Action.INCREMENT:
      return state + 1;
    case Action.DECREMENT:
      return state - 1;
    default:
      return state
  }
}

const store = createStore(reducer);

store.subscribe(() =>
  console.log(store.getState())
);

console.log(increment());
// Action => { type: 'INCREMENT' }

store.dispatch(increment());
// Reducer =>
//  Prev: 0
//  Next: 1

console.log(decrement());
// Action => { type: 'DECREMENT' }

store.dispatch(decrement());
// Reducer =>
//  Prev: 1
//  Next: 0