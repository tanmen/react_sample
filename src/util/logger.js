const _ = require('lodash');
const max_length = 100;
module.exports = (title, message) => {
  if (!(typeof title === 'string')) return;
  const horizon_line_size = Math.floor(((max_length - title.length - 2) / 2));
  const horizon_line = _.chain(new Array(horizon_line_size)).fill('-').join('').value();
  console.log('\n' + _.join([horizon_line, title, horizon_line], ' '));
  if (!_.isEmpty(message)) {
    console.log(_.chain(message).split('').chunk(99).map(a => a.join('')).join('\n').value() + '\n');
  }
};