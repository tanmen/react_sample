const logger = require('../util/logger');
const createActions = require('redux-actions').createActions;


logger('Action STEP3', 'Mapping付きAction');

const pageMapping = (data, page) => ({ data, page });

const { tanmen } = createActions({
  TANMEN: pageMapping,
});


console.log(tanmen({ jobId: 0 }, { current: 0, size: 1 }));
// =>
// { type: 'TANMEN',
//   payload: { data: { jobId: 0 }, page: { current: 0, size: 1 } } }

