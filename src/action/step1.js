const logger = require('../util/logger');
const createActions = require('redux-actions').createActions;


logger('Action STEP1', '正常系');

const { tanmen } = createActions(
  'TANMEN'
);

console.log(tanmen());
// => { type: 'TANMEN' }
console.log(tanmen('A'));
// => { type: 'TANMEN', payload: 'A' }
console.log(tanmen({ data: 'tanmen' }));
// => { type: 'TANMEN', payload: { data: 'tanmen' } }

const { tanTanmen } = createActions(
  'TAN_TANMEN'
);

console.log(tanTanmen());
// => { type: 'TAN_TANMEN' }
console.log(tanTanmen('A'));
// => { type: 'TAN_TANMEN', payload: 'A' }
console.log(tanTanmen({ data: 'tanmen' }));
// => { type: 'TAN_TANMEN', payload: { data: 'tanmen' } }

