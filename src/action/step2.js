const logger = require('../util/logger');
const createActions = require('redux-actions').createActions;


logger('Action STEP2', 'エラー時の挙動');

const { tanmen } = createActions(
  'TANMEN'
);

console.log(tanmen(new Error()));
// =>
// { type: 'TANMEN',
//   error: true,
//   payload: Error }


